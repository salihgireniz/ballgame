﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    private GameManager gm;
    private Slider slider;
    private Transform EndPoint;
    private GameObject Player;

    
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }
    public void Restarter()
    {
        
        gm.speedInc = 1f;
        slider.value = 0f;
        gm.countInc = 1f;
        Destroy(gameObject);
    }
}
