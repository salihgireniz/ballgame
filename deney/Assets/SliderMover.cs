﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderMover : MonoBehaviour
{
    private Slider slider;
    private float sliderCount;
    void Start()
    {
        sliderCount = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().sliderCount;
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        slider.value = 0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        slider.value += sliderCount * 10 * Time.deltaTime;
    }
}
