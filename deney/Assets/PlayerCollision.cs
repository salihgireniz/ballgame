﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EZCameraShake;

public class PlayerCollision : MonoBehaviour
{
    private GameManager gm;
    public GameObject ReplayButton;
    private GameObject Player;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
            gm.speedInc = 0f;
            gm.countInc = 0f;
            gm.sliderCount = 0f;
            StartCoroutine(Wait());
        }

    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Player.transform.position = Player.GetComponent<move>().playerPos;
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject ContinueButton = Instantiate(ReplayButton, new Vector3(0, 130, 1), Quaternion.identity) as GameObject;
        ContinueButton.transform.SetParent(Canvas.transform, false);
    }
}
